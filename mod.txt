{
    "name" : "BetaFix",
    "description" : "An attempt to rebalance skills and skilltrees. Based on Rokk's",
    "author" : "Jestersage",
    "contact" : "http://steamcommunity.com/id/Jestersage/",
    "updates" : [
        {
            "revision" : 3,
            "identifier" : "betafix",
            "install_folder" : "BetaFix"
        }
    ],
    "hooks" : [  
        {
            "hook_id" : "lib/tweak_data/skilltreetweakdata",
            "script_path" : "skilloverhaul.lua"
        },
		{
			"hook_id" : "lib/tweak_data/upgradestweakdata",
			"script_path" : "skilloverhaul.lua"
		},
		{
			"hook_id" : "lib/tweak_data/interactiontweakdata",
			"script_path" : "skilloverhaul.lua"
		},
		{
			"hook_id" : "lib/tweak_data/equipmentstweakdata",
			"script_path" : "skilloverhaul.lua"
		},
        {
            "hook_id" : "lib/units/beings/player/states/playerbleedout",
            "script_path" : "skilloverhaul.lua"
        },
        {
            "hook_id" : "lib/units/enemies/cop/copdamage",
            "script_path" : "skilloverhaul.lua"
        },
        {
            "hook_id" : "lib/units/enemies/cop/copbrain",
            "script_path" : "skilloverhaul.lua"
        },
        {
            "hook_id" : "lib/managers/group_ai_states/groupaistatebase",
            "script_path" : "skilloverhaul.lua"
        },
        {
            "hook_id" : "lib/units/weapons/raycastweaponbase",
            "script_path" : "skilloverhaul.lua"
        },
		{
			"hook_id" : "lib/managers/playermanager",
			"script_path" : "skilloverhaul.lua"
		},
		{
			"hook_id" : "lib/managers/localizationmanager",
			"script_path" : "skilloverhaul.lua"
		},
        {
			"hook_id" : "lib/units/beings/player/playerdamage",
			"script_path" : "skilloverhaul.lua"
		},
        {
			"hook_id" : "lib/units/equipment/sentry_gun/sentrygunbrain",
			"script_path" : "skilloverhaul.lua"
		},
        {
			"hook_id" : "lib/units/weapons/sentrygunweapon",
			"script_path" : "skilloverhaul.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/states/playerstandard",
			"script_path" : "skilloverhaul.lua"
		},
		{
			"hook_id" : "lib/network/handlers/unitnetworkhandler",
			"script_path" : "skilloverhaul.lua"
		}
    ]
}