--Cheat if you want; don't associate your name with mine. Or rokk's

Hooks:PostHook(UpgradesTweakData, "_init_pd2_values", "SkillOverhaulUpgradeValues", function(self)

    -- ;)
	self.values.player.melee_kill_always_snatch_pager = {true}

    --Dominator ace
    self.values.player.threat_intimidate = {true}
	
    --Tripmine and perk deck stuff
    if SkillOverhaul.MenuData.SC then
        
        --Technically, I am just "restoring" right? ;-)
        --Doesn't work for now.
        --self.values.player.run_dodge_chance = {0.25}
        
        if not SkillOverhaul.MenuData.DisableTripmineTweaks then
            --disable no matter what. Post 100 this is not needed
            --self.values.trip_mine.quantity_1 = {4}
            --self.values.trip_mine.quantity_3 = {6}
        end
        --self.values.player.long_dis_revive = {1, 1}  --have to take a balance between 100% every 20 sec or 85% every second. Kept from Rokk to prevent issues. Retain within this area
        
        --self.values.player.passive_dodge_chance = {
        --    0.1,
        --    0.2,
        --    0.3
        --}
        --self.values.smg.damage_multiplier = {1.2}
        
        --these are actually nerfs
        --nerf the speed to simulate crouch
        self.values.player.body_armor.movement = {
            1.05,
            1.025,
            1,
            0.95,
            0.75,
            0.65,
            0.575
        }
        
        --nerf stamina
        self.values.player.body_armor.stamina = {
            1.025,
            1,
            0.95,
            0.9,
            0.85,
            0.8,
            0.7
        }
        
    end
    
    --no longer needed??
    --[[
    self.values.player.passive_dodge_chance = {
        0.1,
        0.25,
        0.4
    }
    ]]--
    
    --For exprez, I think.
    self.values.player.level_2_dodge_addend = {
		0.1,
		0.2,
		0.35
	}
	self.values.player.level_3_dodge_addend = {
		0.1,
		0.2,
		0.35
	}
	self.values.player.level_4_dodge_addend = {
		0.1,
		0.2,
		0.35
	}
    
    --original use {1, 20}
    --have to take a balance between 100% every 20 sec, or 85% every second. 
    self.values.player.long_dis_revive = {1, 1}  --Kept from Rokk to prevent issues
    self.values.cooldown.long_dis_revive = {
		{1, 1}
	}
    
    --Shockproof Ace
	--self.taser_malfunction_max = 5 --beta is at 30, from rokk
    --self.values.player.electric_bullets_while_tased = {true} --not in beta, from rokk
    
    --Shotgun CQB Ace
    --self.values.shotgun.fire_rate_multiplier = {1.5} --not in beta, from rokk
    
    --Tough Guy Ace
    self.values.player.bleed_out_health_multiplier = {2.5}
    
    --Hard Boiled Ace
    --self.values.lmg.damage_multiplier = {1.2} --not in beta
    
    --Hard Boiled Basic
    self.values.weapon.hip_fire_spread_index_addend = {2}
    self.values.shotgun.hip_fire_spread_index_addend = {2}
    
    --Brother's Keeper
    self.values.first_aid_kit.downs_restore_chance = {0.25}
    
    --Overkill; not in beta
    --[[self.values.temporary.overkill_damage_reduction = {
        {0.8, 5}
    }]]-- 
    
    --Moving Target
	self.values.player.detection_risk_add_movement_speed = {
		{
			0.02,
			3,
			"below",
			35,
			0.2
		},
		{
			0.02,
			1,
			"below",
			35,
			0.3
		}
	}
    
    --Trigger Happy    
    --self.values.akimbo.stacking_hit_damage_multiplier = {0.1} not in beta
    --self.values.akimbo.stacking_hit_expire_t = {2, 8}
 
    --unknown, was in older one. Maybe bad idea?
    self.values.sentry_gun.damage_multiplier = {4}
 
    --De-nerf fix
    self.values.player.regain_throwable_from_ammo = {
		{chance = 0.3, chance_inc = 1.1}
	}
    
    self.values.shotgun.damage_multiplier = {1.35, 1.45}
    
    --Muscle self heal
    self.values.player.passive_health_regen = {0.05} --proper pre nerf = 0.05 read somewhere as such even though wiki say 4%?
    
    --Grinder durations
    --Damaging an enemy heals 5 life points every 0.5 seconds for 6 seconds.
    --This effect stacks but cannot occur more than once every 1 seconds and only while wearing the two-piece suit or lightweight ballistic vest.
    self.damage_to_hot_data = {
		armors_allowed = {"level_1", "level_2"},
		works_with_armor_kit = true,
		tick_time = 0.5, --release: 0.5. 0.33 to match biker?
		total_ticks = 12, --release: 6 sec. 6/0.5 = 12. 18 to match biker?
		max_stacks = false,
		stacking_cooldown = 1, --release: 1. 0.83 to match biker?
		add_stack_sources = {
			bullet = true,
			explosion = true,
			melee = true,
			taser_tased = true,
			poison = true,
			fire = true,
			swat_van = true,
			civilian = true
		}
	}
    
    self.values.player.damage_to_hot = {
		0.2,
		0.3,
		0.4,
		0.5
	}
    self.values.player.damage_to_hot_extra_ticks = {2}
    
    --dodge values. Integrate the duck and cover into the OLD values
	self.values.player.body_armor.dodge = {      
        0.25,  --was  0.05, old  0.10; take OLD. max = 0.1+0.25 = 0.35  crouch: 0.1+0.15= 0.25 since sprint only +0.1, 0.35-0.1=0.25
		0.10,  --was -0.05, old -0.20; take NEW. max = -0.05+0.25 = 0.20  crouch: -0.05+0.15 = 0.1 since sprint only +0.1, 0.2-0.1=0.1
		-0.10, --was -0.10, old -0.25; take NEW. max = -0.10+0.25 = 0.15  crouch = -0.10+0.15 = 0.05
		-0.15, --was -0.15, old -0.30; take NEW. max = -0.30 + 0.25 = 0
		-0.2,  --was -0.20, old -0.35; take NEW.  = -0.35
		-0.25, --was -0.25, old -0.40; take NEW.  = -0.4
		-0.35, --was -0.55, old -0.50; take OLD. old crouch  = -0.5+0.15= -0.35
	}
    
    --currently is 0.1. Once I figure out how to implement proper duck and cover, this will be uncommented. Though technically, I am just "restoring" right? ;-)
    --self.values.player.run_dodge_chance = {0.25} 
    
    self.values.player.armor_multiplier = {1.5}
    
    self.values.player.headshot_regen_armor_bonus = {1.5, 4.5}
    
    --store health
    self.values.player.body_armor.skill_max_health_store = {
		22, --was 14 (22 = max)
		15, --was 13.5
		14.5, --was 12.5
		14, --was 12
		13.5, --was 10.5
		13, --was 9.5
		12 --was 4
	}
    
    --denerfing of ammo pickup is in raycastweaponbase, add_ammo.
    
    
    self.values.player.body_armor.skill_kill_change_regenerate_speed = {
        30, --was 14,
		22, --was 13.5
		14, --was 12.5
		13.5, --was 12
		13, --was 10.5
		12.5, --was 9.5
		11.5 --was 4
	}
    

    -- from long guide:
    --"Every 2/3/4/5/6/7/10 s (for suit/light vest/...):
    --Regenerate 10/15/20/25/35/45/85 armor.
    --best is high value for first, low for second
    --u100 is better
    --[[
    self.values.player.armor_grinding = {
		{
			{1, 2},
			{1.5, 3},
			{2, 4},
			{2.5, 5},
			{3.5, 6},
			{4.5, 7},
			{8.5, 10}
		}
	}
    
    ]]--
    
end)

Hooks:PostHook(UpgradesTweakData, "_player_definitions", "SkillOverhaulPlayerDefinitions", function(self)

	self.definitions.player_melee_kill_always_snatch_pager = {
		category = "feature",
		name_id = "menu_player_melee_kill_always_snatch_pager",
		upgrade = {
			category = "player",
			upgrade = "melee_kill_always_snatch_pager",
			value = 1
		}
	}
    
	self.definitions.player_electric_bullets_while_tased = {
		category = "feature",
		name_id = "menu_player_eletric_bullets_while_tased",
		upgrade = {
			category = "player",
			upgrade = "electric_bullets_while_tased",
			value = 1
		}
	}
    
    self.definitions.player_threat_intimidate = {
		category = "feature",
		name_id = "menu_player_threat_intimidate",
		upgrade = {
			category = "player",
			upgrade = "threat_intimidate",
			value = 1
		}
	}
    
    self.definitions.shotgun_fire_rate_multiplier = {
		category = "feature",
		name_id = "menu_shotgun_rof_multiplier",
		upgrade = {
			category = "shotgun",
			upgrade = "fire_rate_multiplier",
			value = 1
		}
	}
    
	self.definitions.lmg_damage_multiplier = {
		category = "feature",
		name_id = "menu_lmg_damage_multiplier",
		upgrade = {
			category = "lmg",
			upgrade = "damage_multiplier",
			value = 1
		}
	}
    
    self.definitions.player_overkill_damage_reduction = {
        category = "temporary",
        name_id = "menu_player_overkill_damage_reduction",
        upgrade = {
            category = "temporary",
            upgrade = "overkill_damage_reduction",
            value = 1
        }
    }
    
    if not SkillOverhaul.MenuData.SC then
    
        self.definitions.player_detection_risk_add_movement_speed_1 = {
            category = "feature",
            name_id = "menu_player_detection_risk_add_movement_speed",
            upgrade = {
                category = "player",
                upgrade = "detection_risk_add_movement_speed",
                value = 1
            }
        }
        
        self.definitions.player_detection_risk_add_movement_speed_2 = {
            category = "feature",
            name_id = "menu_player_detection_risk_add_movement_speed",
            upgrade = {
                category = "player",
                upgrade = "detection_risk_add_movement_speed",
                value = 2
            }
        }
        
        self.definitions.smg_damage_multiplier_1 = {
            category = "feature",
            name_id = "menu_smg_damage_multiplier_1",
            upgrade = {
                category = "smg",
                upgrade = "damage_multiplier",
                value = 1
            }
        }
    
    end
    
	self.definitions.akimbo_stacking_hit_damage_multiplier = {
		category = "feature",
		name_id = "menu_pistol_stacking_hit_damage_multiplier",
		upgrade = {
			category = "akimbo",
			upgrade = "stacking_hit_damage_multiplier",
			value = 1
		}
	}
	self.definitions.akimbo_stacking_hit_expire_t_1 = {
		category = "feature",
		name_id = "menu_akimbo_stacking_hit_expire_t",
		upgrade = {
			category = "akimbo",
			upgrade = "stacking_hit_expire_t",
			value = 1
		}
	}
	self.definitions.akimbo_stacking_hit_expire_t_2 = {
		category = "feature",
		name_id = "menu_akimbo_stacking_hit_expire_t",
		upgrade = {
			category = "akimbo",
			upgrade = "stacking_hit_expire_t",
			value = 2
		}
	}

end)